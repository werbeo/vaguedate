package org.vergien.vaguedate.validation;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.*;

import javax.validation.*;

@Target({ METHOD, FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = PastValidator.class)
@Documented
public @interface Past
{

   String message() default "{is in the future}";

   Class<?>[] groups() default {};

   Class<? extends Payload>[] payload() default {};

}