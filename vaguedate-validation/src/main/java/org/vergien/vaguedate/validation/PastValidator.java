package org.vergien.vaguedate.validation;

import java.time.LocalDate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.infinitenature.vaguedate.VagueDate;

public class PastValidator implements ConstraintValidator<Past, VagueDate>
{

   @Override
   public void initialize(Past constraintAnnotation)
   {


   }

   @Override
   public boolean isValid(VagueDate value, ConstraintValidatorContext context)
   {
      if (value == null)
      {
	 return true;
      }

      if (value.getStartDate().isAfter(LocalDate.now()))
      {
	 return false;
      }
      else
      {
	 return true;
      }
   }

}
