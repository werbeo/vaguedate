package org.vergien.vaguedate.validation;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import javax.validation.Validation;
import javax.validation.Validator;

import org.infinitenature.vaguedate.VagueDate;
import org.infinitenature.vaguedate.VagueDateFactory;
import org.junit.Test;

public class TestVagueDateValidation
{
   Validator v = Validation.buildDefaultValidatorFactory().getValidator();

   class ValidationTest
   {
      @Past
      private VagueDate vagueDate;

      public ValidationTest(VagueDate vagueDate)
      {
	 super();
	 this.vagueDate = vagueDate;
      }

      public VagueDate getVagueDate()
      {
	 return vagueDate;
      }

      public void setVagueDate(VagueDate vagueDate)
      {
	 this.vagueDate = vagueDate;
      }

   }

   @Test
   public void testPast()
   {
      ValidationTest inTheFuture = new ValidationTest(
	    VagueDateFactory.fromDate(LocalDate.now().plusDays(2)));
      ValidationTest today = new ValidationTest(
	    VagueDateFactory.fromDate(LocalDate.now()));
      ValidationTest inThePast = new ValidationTest(
	    VagueDateFactory.fromDate(LocalDate.now().minusDays(2)));

      assertEquals(1, v.validate(inTheFuture).size());
      assertEquals(0, v.validate(inThePast).size());
      assertEquals(0, v.validate(today).size());
   }
}
