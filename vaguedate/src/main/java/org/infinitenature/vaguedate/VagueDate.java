package org.infinitenature.vaguedate;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of the vaugeDate type from indicia
 * <p>
 * A vagueDate can represent a day or a period of time
 *
 * @see <a
 *      href="http://code.google.com/p/indicia/source/browse/core/trunk/application/helpers/vague_date.php">vague_date.php</a>
 *
 * @author dve
 *
 */
public class VagueDate implements Comparable<VagueDate>
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(VagueDate.class);
   private static String DAY = "D";
   private static String TO_YEAR = "-Y";
   private static String FROM_YEAR = "Y-";
   private static String YEAR = "Y";
   private static String YEARS = "YY";
   private static String MONTH_IN_YEAR = "O";
   private static String DAYS = "DD";

   public enum Type
   {
      DAY(VagueDate.DAY), DAYS(VagueDate.DAYS), MONTH_IN_YEAR(
	    VagueDate.MONTH_IN_YEAR), YEAR(VagueDate.YEAR), TO_YEAR(
	    VagueDate.TO_YEAR), FROM_YEAR(VagueDate.FROM_YEAR), YEARS(
	    VagueDate.YEARS);
      private String string;

      private Type(String string)
      {
	 this.string = string;
      }

      public String getStringRepresentation()
      {
	 return string;
      }

      public static final Map<String, Type> typeMapping;
      static
      {
	 final HashMap<String, Type> myMap = new HashMap<String, VagueDate.Type>();
	 myMap.put(VagueDate.DAY, DAY);
	 myMap.put(VagueDate.DAYS, DAYS);
	 myMap.put(VagueDate.TO_YEAR, TO_YEAR);
	 myMap.put(VagueDate.YEAR, YEAR);
	 myMap.put(VagueDate.MONTH_IN_YEAR, MONTH_IN_YEAR);
	 myMap.put(VagueDate.FROM_YEAR, FROM_YEAR);
	 myMap.put(VagueDate.YEARS, YEARS);

	 typeMapping = Collections.unmodifiableMap(myMap);
      };

      public static Type get(String typeString)
      {
	 return typeMapping.get(typeString);
      }

   }

   private LocalDate startDate;
   private LocalDate endDate;
   private Type type;

   private static final DateTimeFormatter dayPattern = DateTimeFormatter.ofPattern("yyyy-MM-dd");
   private static final DateTimeFormatter yearPattern = DateTimeFormatter.ofPattern("yyyy");
   private static final DateTimeFormatter monthPattern = DateTimeFormatter.ofPattern("yyyy-MM");
   private static final DateTimeFormatter toYearPattern = DateTimeFormatter.ofPattern("-yyyy");
   private static final DateTimeFormatter fromYearPattern = DateTimeFormatter.ofPattern("yyyy-");

   public VagueDate(LocalDate start, LocalDate end, Type type)
   {
      super();

      if (type == null)
      {
	 LOGGER.error("Type for VagueDate is null!", new Throwable());
      }

      this.startDate = start;
      this.endDate = end;
      this.type = type;
   }

   public VagueDate(LocalDate start, LocalDate end, String dateType)
   {
      this(start, end, Type.get(dateType));
   }

   @Override
   public String toString()
   {
      switch (type)
      {
      case DAY:
	 return startDate.format(dayPattern);
      case MONTH_IN_YEAR:
	 return startDate.format(monthPattern);
      case TO_YEAR:
	 return endDate.format(toYearPattern);
      case YEAR:
	 return startDate.format(yearPattern);
      case DAYS:
	 return startDate.format(dayPattern) + " to "
	       + endDate.format(dayPattern);
      case FROM_YEAR:
	 return startDate.format(fromYearPattern);
      case YEARS:
	 return startDate.format(yearPattern) + " to "
	       + endDate.format(yearPattern);
      default:
	 return "unkonwn Type";
      }
   }

   /**
    * Gets the precision of a vagueDate in days.
    *
    * @return the range this vagueDate covers in days. {@code Integer.MAX_VALUE}
    *         if this vagueDate has an open beginning or end
    */
   public int getPrecision()
   {
      if (startDate != null && endDate != null)
      {
         return (int) (java.time.temporal.ChronoUnit.DAYS.between(startDate, endDate) + 1);
      } else
      {
         return Integer.MAX_VALUE;
      }
   }

   public Type getType()
   {
      return this.type;
   }

   public LocalDate getEndDate()
   {
      return endDate;
   }

   public LocalDate getStartDate()
   {
      return startDate;
   }

   @Override
   public int hashCode()
   {
      return new HashCodeBuilder().append(startDate).append(endDate)
	    .append(type).toHashCode();
   }

   @Override
   public boolean equals(Object obj)
   {
      if (obj == null)
      {
	 return false;
      }
      if (obj == this)
      {
	 return true;
      }
      if (obj.getClass() != getClass())
      {
	 return false;
      }
      VagueDate rhs = (VagueDate) obj;
      return new EqualsBuilder().append(this.startDate, rhs.startDate)
	    .append(this.endDate, rhs.endDate).append(this.type, rhs.type)
	    .isEquals();
   }

   @Override
   public int compareTo(VagueDate o)
   {
      return new CompareToBuilder().append(this.startDate, o.startDate)
	    .append(this.endDate, o.endDate).append(this.type, o.type)
	    .toComparison();
   }

   protected boolean hasEndDate()
   {
      return endDate == null ? false : true;
   }

   protected boolean hasStartDate()
   {
      return startDate == null ? false : true;
   }
}