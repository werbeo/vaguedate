package org.infinitenature.vaguedate;

import java.time.LocalDate;
import java.time.Year;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.vaguedate.VagueDate.Type;

public class VagueDateFactory
{
   /**
    * Creates a vagueDate from String representation from indica
    *
    * @param startDate
    *           (yyyy-MM-dd) or null
    * @param endDate
    *           (yyyy-MM-dd) or null
    * @param dateType
    * @return
    */
   private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

   public static VagueDate create(String startDate, String endDate,
	 String dateType)
   {
      LocalDate start = null;
      LocalDate end = LocalDate.parse(endDate, formatter);
      if (StringUtils.isNotBlank(startDate))
      {
	 start = LocalDate.parse(startDate, formatter);
      }
      return new VagueDate(start, end, dateType);
   }

   /**
    * Creates a vagueDate representing a day
    *
    * @param date
    * @return
    */
   public static VagueDate fromDate(LocalDate date)
   {
      return new VagueDate(date, date, VagueDate.Type.DAY);
   }

   /**
    * Creates a vagueDate representing a month in a year
    *
    * @param year
    * @param month
    *           the month of the year, one based
    * @return
    */
   public static VagueDate createMonthInYear(int year, int month)
   {
      YearMonth yearMonth = YearMonth.of(year, month);
      LocalDate startDate = yearMonth.atDay(1);
      LocalDate endDate = yearMonth.atEndOfMonth();
      return new VagueDate(startDate, endDate, Type.MONTH_IN_YEAR);
   }

   public static VagueDate createMonthInYear(YearMonth yearMonth)
   {
      LocalDate startDate = yearMonth.atDay(1);
      LocalDate endDate = yearMonth.atEndOfMonth();
      return new VagueDate(startDate, endDate, Type.MONTH_IN_YEAR);
   }

   /**
    * Creates a vagueDate representing a year
    *
    * @param year
    * @return
    */
   public static VagueDate create(int year)
   {
      Year yearJavaTime = Year.of(year);
      LocalDate startDate = yearJavaTime.atDay(1);
      LocalDate endDate = yearJavaTime.atMonth(12).atEndOfMonth();
      return new VagueDate(startDate, endDate, Type.YEAR);
   }

   /**
    * Creates a vagueDate representing a period to and including a year
    *
    * @param year
    * @return
    */
   public static VagueDate createTo(int year)
   {
      LocalDate endDate = lastDayOfYear(year);
      return new VagueDate(null, endDate, Type.TO_YEAR);
   }

   private static LocalDate lastDayOfYear(int year)
   {
      return Year.of(year).atMonth(12).atEndOfMonth();
   }

   private static LocalDate firstDayOfYear(int year)
   {
      return Year.of(year).atMonth(1).atDay(1);
   }

   public static VagueDate create(LocalDate startDate, LocalDate endDate, String dateType)
   {
      return new VagueDate(startDate, endDate, dateType);
   }

   /**
    *
    * @param date
    * @param type
    * @return
    */
   public static VagueDate create(LocalDate date, Type type)
   {
      if (type == null || date == null)
      {
	 return null;
      }
      else
      {
	 switch (type)
	 {
	 case DAY:
	    return fromDate(date);
	 case MONTH_IN_YEAR:
	    return createMonthInYear(date.getYear(),
		  date.getMonthValue());
	 case YEAR:
	    return create(date.getYear());
	 case TO_YEAR:
	    return createTo(date.getYear());
	 default:
	    throw new IllegalArgumentException("Type " + type
		  + " not implemented yet");
	 }
      }
   }

   public static VagueDate create(YearMonth yearMonth, Type type)
   {
      if(Type.MONTH_IN_YEAR == type)
      {
         return createMonthInYear(yearMonth);
      }
      throw new IllegalArgumentException("Type" +" " + type
            + " is not "+ Type.MONTH_IN_YEAR);
   }

   public static VagueDate create(Year year, Type type)
   {
      switch(type)
      {
      case YEAR:
         return create(year.getValue());
      case TO_YEAR:
         return createTo(year.getValue());
      case FROM_YEAR:
         return createFrom(year.getValue());
         default:
            throw new IllegalArgumentException("Type " + type
                  + " not allowed for Year");
      }
   }

   public static VagueDate create(int fromYear, int toYear)
   {
      if (fromYear != 0 && toYear != 0 && fromYear != toYear)
      {
	 return new VagueDate(firstDayOfYear(fromYear), lastDayOfYear(toYear),
	       Type.YEARS);
      }
      else if (fromYear == toYear && fromYear != 0)
      {
	 return create(fromYear);
      }
      else if (fromYear != 0)
      {
	 return createFrom(fromYear);
      }
      else if (toYear != 0)
      {
	 return createTo(toYear);
      }
      else
      {
	 return null;
      }
   }

   public static VagueDate createFrom(int fromYear)
   {
      return new VagueDate(firstDayOfYear(fromYear), null, Type.FROM_YEAR);
   }

   /**
    * Creates a new VagueDate of the specified day.
    *
    * @param year
    * @param month
    * @param day
    * @return
    */
   public static VagueDate create(int year, int month, int day)
   {
      return new VagueDate(LocalDate.of(year, month, day),
            LocalDate.of(year, month, day), VagueDate.Type.DAY);
   }
}