package org.infinitenature.vaguedate;

import java.time.format.DateTimeFormatter;

import org.infinitenature.vaguedate.VagueDate.Type;

public class VagueDateHelper
{
   private VagueDateHelper()
   {
      throw new IllegalAccessError("Utility class");
   }

   /**
    * Formats a VagueDate to a human readable string.
    *
    * @param date
    * @return
    */
   public static String format(VagueDate date)
   {
      if (date.getType() == Type.DAY)
      {
         return date.getEndDate()
               .format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
      } else if (date.getType() == Type.MONTH_IN_YEAR)
      {
         return date.getEndDate()
               .format(DateTimeFormatter.ofPattern("MM.yyyy"));
      } else if (date.getType() == Type.YEAR)
      {
         return date.getEndDate().format(DateTimeFormatter.ofPattern("yyyy"));
      } else if (date.getType() == Type.TO_YEAR)
      {
         return "bis "
               + date.getEndDate().format(DateTimeFormatter.ofPattern("yyyy"));
      } else
      {
         return date.toString();
      }
   }
}
