package org.infinitenature.vaguedate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;

import org.infinitenature.vaguedate.VagueDateFactory;
import org.junit.jupiter.api.Test;

public class TestVagueDate
{

   @Test
   public void testCompareTo()
   {
      assertThat(VagueDateFactory.create(1900)
            .compareTo(VagueDateFactory.create(1900)), is(0));
      assertThat(VagueDateFactory.create(1800)
            .compareTo(VagueDateFactory.create(1900)), lessThan(0));

      assertThat(VagueDateFactory.create(1900)
            .compareTo(VagueDateFactory.create(1800)), greaterThan(0));
   }

   @Test
   public void testHasStartDate()
   {
      assertThat(VagueDateFactory.create(1900).hasStartDate(), is(true));
   }

   @Test
   public void testHasEndDate()
   {
      assertThat(VagueDateFactory.create(1900).hasEndDate(), is(true));
   }
}
