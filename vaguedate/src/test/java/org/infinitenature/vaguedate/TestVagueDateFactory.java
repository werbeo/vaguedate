package org.infinitenature.vaguedate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.concurrent.CountDownLatch;

import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestVagueDateFactory
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(TestVagueDateFactory.class);

   @Test
   public void testCreateIntInt()
   {
      VagueDate year2year = VagueDateFactory.create(1981, 1988);
      assertEquals(VagueDate.Type.YEARS, year2year.getType());
      assertEquals("1981-01-01", year2year.getStartDate().toString());
      assertEquals("1988-12-31", year2year.getEndDate().toString());

      VagueDate fromYear = VagueDateFactory.create(1981, 0);
      assertEquals(VagueDate.Type.FROM_YEAR, fromYear.getType());
      assertEquals("1981-01-01", fromYear.getStartDate().toString());
      assertNull(fromYear.getEndDate());

      VagueDate toYear = VagueDateFactory.create(0, 1988);
      assertEquals(VagueDate.Type.TO_YEAR, toYear.getType());
      assertNull(toYear.getStartDate());
      assertEquals("1988-12-31", toYear.getEndDate().toString());

      VagueDate year = VagueDateFactory.create(1981, 1981);
      assertEquals(VagueDate.Type.YEAR, year.getType());
      assertEquals("1981-01-01", year.getStartDate().toString());
      assertEquals("1981-12-31", year.getEndDate().toString());

      assertNull(VagueDateFactory.create(0, 0));
   }

   @Test
   @Ignore
   public void testMultiThread() throws InterruptedException
   {
      final int threads = 100;
      final int triesByThread = 9000000;
      final CountDownLatch countDownLatch = new CountDownLatch(
            threads * triesByThread);
      Runnable factoryTest = new Runnable()
      {
         private int turns = 0;

         @Override
         public void run()
         {
            LOGGER.error("Start thread" + Thread.currentThread().getName());
            while (turns < triesByThread)
            {
               try
               {
                  VagueDate date = VagueDateFactory.create("1981-01-01",
                        "1981-01-01", "D");
                  assertNotNull(date.getType());
                  VagueDate date2 = VagueDateFactory.create("1981-01-01",
                        "1981-10-10", "DD");
                  assertNotNull(date2.getType());
                  Thread.sleep(20);
               } catch (Exception e)
               {
                  LOGGER.error("Failure crating vagueDate", e);
                  fail("No exception expected");
               }
               turns++;
            }
            countDownLatch.countDown();
            LOGGER.error("Stop thread" + Thread.currentThread().getName());
         }
      };

      for (int i = 0; i < threads; i++)
      {
         Thread thread = new Thread(factoryTest);

         thread.start();
      }
      countDownLatch.await();
   }
}
